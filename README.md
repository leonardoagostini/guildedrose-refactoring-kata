# Project structure

Here is the project structure

```commandline
|-- expected_result_guilded_rose.py # list of expected results for tests
|-- guilded_rose.py # main code
|-- test_guilded_rose.py # implemented tests
|-- README.md
|-- texttest_fixture.py
```

# Refactoring steps

## 1. Creating test

First thing I did was to create a comprehensive test file that would test all the info stated in 
`GildedRoseRequirements.txt`. I focused the testin in:

* Check how quality changes for a generic item named `dummy`;
* Check how quality changes for `Aged Brie`;
* Check how quality changes for `Backstage passes to a TAFKAL80ETC concert`;
* Check how quality changes for `Sulfuras, Hand of Ragnaros`;
* Check what happens if `Sulfuras, Hand of Ragnaros` has a quality different from `80`;
* Check what happens if `updated_quality` is called wit lowercase words;
* Check what happens if `updated_quality` is called wit uppercase words;

During the implementation of test I discovered the following:

* `Aged Brie` improves quality twice as fast after the `sell-in` date. This is not stated in the 
`GildedRoseRequirements.txt` therefore I consider it an error.
* `Backstage passes` different to `Backstage passes to a TAFKAL80ETC concert` are not considered as `Backstage passes`
* There is no control on the quality of `Sulfuras, Hand of Ragnaros` even if it should be only 80.
* There is no control on how the item name is written: if the item name is in lower case or in upper case it is not
recognized.

## 2 Refactoring and improvements

After having studied the tests results, I worked on the `update_quality` code trying to use the existing code as a
backbone of the new code. The main thing I changed were:

* Adding a check if the item is `Sulfuras`: if it is the case the code will only check that the quality is the wanted
one but no more computation are required;
* Add a more resilient way to check the item name with a trim-lowercase logic;
* Avoid the usage of various if-else statement to control and take advantage of mathematical functions.
