# -*- coding: utf-8 -*-

class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            uniformed_item_name = ''.join(item.name.split()).lower()
            if uniformed_item_name == "sulfuras,handofragnaros":
                if item.quality != 80:
                    print("Sulfuras must have quality equal to 80! I change it for you!")
                    item.quality = 80
            else:
                item.sell_in = item.sell_in - 1
                sign_of_int = lambda i: 1 if i >= 0 else -1
                quality_update_factor = 1

                if uniformed_item_name != "agedbrie" and "backstagepasses" not in uniformed_item_name:
                    if "conjured" in uniformed_item_name:
                        quality_update_factor = 2

                    item.quality = int(max(item.quality - quality_update_factor*(1 + (1 - sign_of_int(item.sell_in))
                                                                                 / 2), 0))
                else:
                    item.quality = min(item.quality + 1, 50)
                    if "backstagepasses" in uniformed_item_name:
                        if item.sell_in < 0:
                            item.quality -= item.quality
                        else:
                            if item.sell_in < 10:
                                item.quality = min(item.quality + 1, 50)
                            if item.sell_in < 5:
                                item.quality = min(item.quality + 1, 50)


class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)
