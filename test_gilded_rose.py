# -*- coding: utf-8 -*-
import unittest
from typing import Dict, List

from gilded_rose import Item, GildedRose
from expected_results_gileded_rose import QUALITY_DEGRADATION_DUMMY, QUALITY_DEGRADATION_AGED_BRIE, \
    QUALITY_DEGRADATION_BACKSTAGE, QUALITY_DEGRADATION_SULFURAS, QUALITY_DEGRADATION_CONJURED, \
    CONSTANT_QUALITY_SULFURAS


def quality_passing_days(items: List[Item], days_to_pass: int) -> Dict[str, Dict[int, Dict[str, int]]]:
    gilded_rose = GildedRose(items)
    quality_variation = {item.name: {} for item in items}

    for day in range(0, days_to_pass):
        gilded_rose.update_quality()

        for item in items:
            quality_variation[item.name][day + 1] = {"sell_in": item.sell_in,
                                                     "quality": item.quality}

    return quality_variation


class GildedRoseTest(unittest.TestCase):

    def test_quality_degradation_general(self):
        items = [Item("dummy", 5, 15)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertEqual(quality_variation, QUALITY_DEGRADATION_DUMMY)

    def test_quality_degradation_aged_brie(self):
        items = [Item("Aged Brie", 5, 40)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertEqual(quality_variation, QUALITY_DEGRADATION_AGED_BRIE)

    def test_quality_degradation_backstage_passes(self):
        items = [Item("Backstage passes to a TAFKAL80ETC concert", 15, 10),
                 Item("Backstage passes to a GENERIC concert", 15, 10)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertEqual(quality_variation, QUALITY_DEGRADATION_BACKSTAGE)

    def test_quality_degradation_sulfuras(self):
        items = [Item("Sulfuras, Hand of Ragnaros", 10, 80)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertEqual(quality_variation, QUALITY_DEGRADATION_SULFURAS)

    def test_quality_of_sulfuras_only_eighty(self):
        items = [Item("Sulfuras, Hand of Ragnaros", 10, 30)]

        days_to_pass = 1

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertNotEqual(quality_variation, CONSTANT_QUALITY_SULFURAS)

    def test_quality_with_lowercase_name(self):
        items = [Item("aged brie", 5, 40),
                 Item("backstage passes to a tafkal80etc concert", 15, 10),
                 Item("sulfuras, hand of ragnaros", 10, 80)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        expected_quality_degradation_lowercase = \
            {
                'aged brie': QUALITY_DEGRADATION_AGED_BRIE["Aged Brie"],
                'backstage passes to a tafkal80etc concert':
                    QUALITY_DEGRADATION_BACKSTAGE["Backstage passes to a TAFKAL80ETC concert"],
                'sulfuras, hand of ragnaros': QUALITY_DEGRADATION_SULFURAS["Sulfuras, Hand of Ragnaros"]
            }

        self.assertEqual(quality_variation, expected_quality_degradation_lowercase)

    def test_quality_with_uppercase_name(self):
        items = [Item("AGED BRIE", 5, 40),
                 Item("BACKSTAGE PASSES TO A TAFKAL80ETC CONCERT", 15, 10),
                 Item("SULFURAS, HAND OF RAGNAROS", 10, 80)]

        days_to_pass = 20

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        expected_quality_degradation_uppercase = \
            {
                'AGED BRIE': QUALITY_DEGRADATION_AGED_BRIE["Aged Brie"],
                'BACKSTAGE PASSES TO A TAFKAL80ETC CONCERT':
                    QUALITY_DEGRADATION_BACKSTAGE["Backstage passes to a TAFKAL80ETC concert"],
                'SULFURAS, HAND OF RAGNAROS': QUALITY_DEGRADATION_SULFURAS["Sulfuras, Hand of Ragnaros"]
            }

        self.assertEqual(quality_variation, expected_quality_degradation_uppercase)


    def test_quality_conjured(self):
        items = [Item("Conjured", 5, 20)]

        days_to_pass = 15

        quality_variation = quality_passing_days(items=items,
                                                 days_to_pass=days_to_pass)

        self.assertEqual(quality_variation, QUALITY_DEGRADATION_CONJURED)


if __name__ == '__main__':
    unittest.main()
