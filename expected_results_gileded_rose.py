from typing import Dict

QUALITY_DEGRADATION_DUMMY: Dict[str, Dict[int, int]] = \
    {
        "dummy":
            {
                1: {"sell_in": 4, "quality": 14},
                2: {"sell_in": 3, "quality": 13},
                3: {"sell_in": 2, "quality": 12},
                4: {"sell_in": 1, "quality": 11},
                5: {"sell_in": 0, "quality": 10},
                6: {"sell_in": -1, "quality": 8},
                7: {"sell_in": -2, "quality": 6},
                8: {"sell_in": -3, "quality": 4},
                9: {"sell_in": -4, "quality": 2},
                10: {"sell_in": -5, "quality": 0},
                11: {"sell_in": -6, "quality": 0},
                12: {"sell_in": -7, "quality": 0},
                13: {"sell_in": -8, "quality": 0},
                14: {"sell_in": -9, "quality": 0},
                15: {"sell_in": -10, "quality": 0},
                16: {"sell_in": -11, "quality": 0},
                17: {"sell_in": -12, "quality": 0},
                18: {"sell_in": -13, "quality": 0},
                19: {"sell_in": -14, "quality": 0},
                20: {"sell_in": -15, "quality": 0}
            }
    }

# aged brie increases its quality twice if it is not stated
QUALITY_DEGRADATION_AGED_BRIE: Dict[str, Dict[int, int]] = \
    {
        "Aged Brie":
            {
                1: {"sell_in": 4, "quality": 41},
                2: {"sell_in": 3, "quality": 42},
                3: {"sell_in": 2, "quality": 43},
                4: {"sell_in": 1, "quality": 44},
                5: {"sell_in": 0, "quality": 45},
                6: {"sell_in": -1, "quality": 46},
                7: {"sell_in": -2, "quality": 47},
                8: {"sell_in": -3, "quality": 48},
                9: {"sell_in": -4, "quality": 49},
                10: {"sell_in": -5, "quality": 50},
                11: {"sell_in": -6, "quality": 50},
                12: {"sell_in": -7, "quality": 50},
                13: {"sell_in": -8, "quality": 50},
                14: {"sell_in": -9, "quality": 50},
                15: {"sell_in": -10, "quality": 50},
                16: {"sell_in": -11, "quality": 50},
                17: {"sell_in": -12, "quality": 50},
                18: {"sell_in": -13, "quality": 50},
                19: {"sell_in": -14, "quality": 50},
                20: {"sell_in": -15, "quality": 50}
            }
    }

QUALITY_DEGRADATION_BACKSTAGE: Dict[str, Dict[int, Dict[str, int]]] = \
    {
        "Backstage passes to a TAFKAL80ETC concert":
            {
                1: {"sell_in": 14, "quality": 11},
                2: {"sell_in": 13, "quality": 12},
                3: {"sell_in": 12, "quality": 13},
                4: {"sell_in": 11, "quality": 14},
                5: {"sell_in": 10, "quality": 15},
                6: {"sell_in": 9, "quality": 17},
                7: {"sell_in": 8, "quality": 19},
                8: {"sell_in": 7, "quality": 21},
                9: {"sell_in": 6, "quality": 23},
                10: {"sell_in": 5, "quality": 25},
                11: {"sell_in": 4, "quality": 28},
                12: {"sell_in": 3, "quality": 31},
                13: {"sell_in": 2, "quality": 34},
                14: {"sell_in": 1, "quality": 37},
                15: {"sell_in": 0, "quality": 40},
                16: {"sell_in": -1, "quality": 0},
                17: {"sell_in": -2, "quality": 0},
                18: {"sell_in": -3, "quality": 0},
                19: {"sell_in": -4, "quality": 0},
                20: {"sell_in": -5, "quality": 0}
            },
    "Backstage passes to a GENERIC concert":
            {
                1: {"sell_in": 14, "quality": 11},
                2: {"sell_in": 13, "quality": 12},
                3: {"sell_in": 12, "quality": 13},
                4: {"sell_in": 11, "quality": 14},
                5: {"sell_in": 10, "quality": 15},
                6: {"sell_in": 9, "quality": 17},
                7: {"sell_in": 8, "quality": 19},
                8: {"sell_in": 7, "quality": 21},
                9: {"sell_in": 6, "quality": 23},
                10: {"sell_in": 5, "quality": 25},
                11: {"sell_in": 4, "quality": 28},
                12: {"sell_in": 3, "quality": 31},
                13: {"sell_in": 2, "quality": 34},
                14: {"sell_in": 1, "quality": 37},
                15: {"sell_in": 0, "quality": 40},
                16: {"sell_in": -1, "quality": 0},
                17: {"sell_in": -2, "quality": 0},
                18: {"sell_in": -3, "quality": 0},
                19: {"sell_in": -4, "quality": 0},
                20: {"sell_in": -5, "quality": 0}
            }
    }

QUALITY_DEGRADATION_SULFURAS: Dict[str, Dict[int, int]] = \
    {
        "Sulfuras, Hand of Ragnaros":
            {
                1: {"sell_in": 10, "quality": 80},
                2: {"sell_in": 10, "quality": 80},
                3: {"sell_in": 10, "quality": 80},
                4: {"sell_in": 10, "quality": 80},
                5: {"sell_in": 10, "quality": 80},
                6: {"sell_in": 10, "quality": 80},
                7: {"sell_in": 10, "quality": 80},
                8: {"sell_in": 10, "quality": 80},
                9: {"sell_in": 10, "quality": 80},
                10: {"sell_in": 10, "quality": 80},
                11: {"sell_in": 10, "quality": 80},
                12: {"sell_in": 10, "quality": 80},
                13: {"sell_in": 10, "quality": 80},
                14: {"sell_in": 10, "quality": 80},
                15: {"sell_in": 10, "quality": 80},
                16: {"sell_in": 10, "quality": 80},
                17: {"sell_in": 10, "quality": 80},
                18: {"sell_in": 10, "quality": 80},
                19: {"sell_in": 10, "quality": 80},
                20: {"sell_in": 10, "quality": 80}
            }
    }

QUALITY_DEGRADATION_CONJURED: Dict[str, Dict[int, int]] = \
    {
        "Conjured":
            {
                1: {"sell_in": 4, "quality": 18},
                2: {"sell_in": 3, "quality": 16},
                3: {"sell_in": 2, "quality": 14},
                4: {"sell_in": 1, "quality": 12},
                5: {"sell_in": 0, "quality": 10},
                6: {"sell_in": -1, "quality": 6},
                7: {"sell_in": -2, "quality": 2},
                8: {"sell_in": -3, "quality": 0},
                9: {"sell_in": -4, "quality": 0},
                10: {"sell_in": -5, "quality": 0},
                11: {"sell_in": -6, "quality": 0},
                12: {"sell_in": -7, "quality": 0},
                13: {"sell_in": -8, "quality": 0},
                14: {"sell_in": -9, "quality": 0},
                15: {"sell_in": -10, "quality": 0}
            }
    }

CONSTANT_QUALITY_SULFURAS = \
    {
        "Sulfuras, Hand of Ragnaros":
            {
                1: {"sell_in": 10, "quality": 30}
            }

    }